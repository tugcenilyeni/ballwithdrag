﻿using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public float count;
    public float countInc = 0;

    public Text levelText;
    public int level;

    public int score;
    public Text scoretext;

    public int amnGroundOnScreen = 10;

    public float speedInc = 0;
    public float speed = 50f;

    public float sliderCount;

    public int highScore;
    public int maxLevel;
    public float maxSpeed;
    public int maxGround;
    public Text highScoreText;
    public Text bestScore;
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        sliderCount = 0f;
        instance = this;
        scoretext.enabled = false;
        bestScore.text = "BEST";
        maxGround = PlayerPrefs.GetInt("MaxGround", 10);
        highScore = PlayerPrefs.GetInt("HighScore", 0);
        maxLevel = PlayerPrefs.GetInt("MaxLevel", level);
        maxSpeed = PlayerPrefs.GetFloat("MaxSpeed", 50F);
        level = maxLevel;
        speed = maxSpeed;
        amnGroundOnScreen = maxGround;
    }
    void Update()
    {
        count += Time.deltaTime * countInc;
        score = (int)count;
        scoretext.text = score.ToString();
        levelText.text = ("LEVEL " + level);
        highScoreText.text = highScore.ToString();
        if(score > highScore)
        {
            PlayerPrefs.SetInt("HighScore", score);
        }
        if(level > maxLevel)
        {
            PlayerPrefs.SetInt("MaxLevel", level);
        }
        if(speed > maxSpeed)
        {
            PlayerPrefs.SetFloat("MaxSpeed", speed);
        }
        if(amnGroundOnScreen > maxGround)
        {
            PlayerPrefs.SetInt("MaxGround", amnGroundOnScreen);
        }
    }
}
