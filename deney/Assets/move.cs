﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    Vector3 objectPos;

    private static move instance;
    private Vector3 moveVector;
    private GameManager gm;
    private Rigidbody rb;
    public float speed;
    public float speedInc;
    public Vector3 playerPos = new Vector3(0f, 0f, 0f);
    

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        instance = this;
        rb = GetComponent<Rigidbody>();;

    }
    void FixedUpdate()
    {
        speed = gm.speed;
        speedInc = gm.speedInc;
        rb.velocity = new Vector3(0f,0f,1f) * -speed * speedInc;
    }
}
