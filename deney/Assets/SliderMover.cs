﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SliderMover : MonoBehaviour
{
    private Slider slider;
    private float sliderCount;
    void Start()
    {
        slider = GameObject.FindGameObjectWithTag("Slider").GetComponent<Slider>();
        slider.value = 0f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        sliderCount = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>().sliderCount;
        slider.value += sliderCount * 5 * Time.deltaTime;
    }
}
